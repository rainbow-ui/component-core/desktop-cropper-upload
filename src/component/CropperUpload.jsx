import { Component, UIDialog, UISelect, CodeTable, UIText, UIMessageHelper } from "rainbowui-desktop-core";
import PropTypes from 'prop-types';
import { PageContext } from "rainbow-desktop-cache";
import { CodeTableService } from "rainbow-desktop-codetable";
import config from "config";
import { Util, r18n } from 'rainbow-desktop-tools';
import '../css/cropper.css';
import Cropper from './react-cropper';
//../img/picture.jpg
const src = ''

export default class CropperUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
        src,
        cropResult: null,
        errMsg: '',
        codetable: [],
        coprBox: [
                { id: '-1', ratio: NaN},
                { id: '1', ratio: 1 / 1},
                { id: '2', ratio: 2 / 3},
                { id: '3', ratio: 4 / 3},
                { id: '4', ratio: 16 / 9},
            ],
        cropRatio: NaN,
        policy: '',
        selectModel: false
        };
        this.createInfo = {
            createType: 1,
            uploadType: 2,
            copyTo: 1,
            reportNum: '',
            uploadNum: '',
            plan: null,
            mainPolicyNo: '',
            orderPolicyNo: '',
            copyText: ''
        };
        showCropButton: false,
        this.cropImage = this.cropImage.bind(this);
        this.onChangeCompress = this.onChangeCompress.bind(this);
        this.useDefaultImage = this.useDefaultImage.bind(this);
        this.useImage = this.useImage.bind(this);
        this.showCropper = 'none';
    }

    componentWillMount () {
        let param = {
            CodeTableName: 'ClauseCategory'
        };
        CodeTableService.getCodeTableValue(param).then((data)=>{
            const a = new CodeTable([
                { id: '-1', text: r18n.cropperCustomMessage},
                { id: '1', text: '1 : 1'},
                { id: '2', text: '2 : 3'},
                { id: '3', text: '4 : 3'},
                { id: '4', text: '16 : 9'},
            ]);
            this.setState({
                codetable: a
            });

        });
    }

    photoCompress(file,obj, callback) {
        var ready=new FileReader();
        /*开始读取指定的Blob对象或File对象中的内容. 当读取操作完成时,readyState属性的值会成为DONE,如果设置了onloadend事件处理程序,则调用之.同时,result属性中将包含一个data: URL格式的字符串以表示所读取文件的内容.*/
        ready.onload=function(){
            var re=this.result;
            // this.canvasDataURL(re,w,objDiv);
            var img = new Image();
            img.src = re;
            img.onload = function(){
                var that = this;
                // 默认按比例压缩
                var w = that.width,
                    h = that.height,
                    scale = w / h;
                w = obj.width || w;
                h = obj.height || (w / scale);
                var quality = 0.7;  // 默认图片质量为0.7
                //生成canvas
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                // 创建属性节点
                var anw = document.createAttribute("width");
                anw.nodeValue = w;
                var anh = document.createAttribute("height");
                anh.nodeValue = h;
                canvas.setAttributeNode(anw);
                canvas.setAttributeNode(anh);
                ctx.drawImage(that, 0, 0, w, h);
                // 图像质量
                if(obj.quality && obj.quality <= 1 && obj.quality > 0){
                    quality = obj.quality;
                }
                // quality值越小，所绘制出的图像越模糊
                var base64 = canvas.toDataURL('image/jpeg', quality);
                // 回调函数返回base64的值
                callback(base64);
            }

        }
        ready.readAsDataURL(file);
    }

    convertBase64UrlToBlob(urlData){
        var arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type:mime});
    }

    onChangeCompress(e) {
        e.preventDefault();
        this.showCropper = "inherit";
        if (this.props.ratioW && this.props.ratioH) {
            this.setState({cropRatio: parseInt(this.props.ratioW) / parseInt(this.props.ratioH)})
        }
        var file = document.getElementById('inputImage');
        if (file.files.length === 0) return;
        let fileSize = file.files[0].size / 1024;
        let maxSize = this.props.maxSize || 1000;
        if (fileSize > maxSize) {
            this.setState({
                errMsg: r18n.imgTooLarge
            });
            $('#croperrmsg').html(r18n.imgTooLarge);
            this.setState({showCropButton: true});
            return;
        }
        var strRegex = '(.jpg|.png|.gif|.jpeg)$'; //用于验证图片扩展名的正则表达式
        var re = new RegExp(strRegex);
        if (!re.test(file.files[0].name.toLowerCase())) {
            this.setState({
                errMsg: r18n.imgNotMatch
            });
            $('#croperrmsg').html(r18n.imgNotMatch);
            this.setState({showCropButton: true})
            return;
        }
        this.setState({
            errMsg: ''
        })

        let files;
        if (e.dataTransfer) {
          files = e.dataTransfer.files;
        } else if (e.target) {
          files = e.target.files;
        }
        let _self = this;
        if (fileSize > 300) { //300k以上压缩上传
            let qu = 0.2;
            if (fileSize < 400) {
                qu = 0.5;
            } else if (fileSize > 400 && fileSize < 800) {
                qu = 0.25;
            } else {
                qu = 0.2;
            }
            this.photoCompress(files[0],{quality: qu}, function(base64Codes){
                _self.setState({ src: base64Codes });
            });
        } else {
            const reader = new FileReader();
            reader.onload = () => {
            this.setState({ src: reader.result });
            };
            reader.readAsDataURL(files[0]);
        }
        this.setState({showCropButton: true})
    }

    // onChange(e) {
    //     e.preventDefault();
    //     var file = document.getElementById('inputImage');
    //     if (file.files.length === 0) return;
    //     let fileSize = file.files[0].size / 1024;
    //     let maxSize = this.props.maxSize || 200;
    //     if (fileSize > maxSize) {
    //         this.setState({
    //             errMsg: '图片太大，请重新选择!'
    //         });
    //         $('#croperrmsg').html('图片太大，请重新选择!');
    //         return;
    //     }
    //     var strRegex = '(.jpg|.png|.gif|.jpeg)$'; //用于验证图片扩展名的正则表达式
    //     var re = new RegExp(strRegex);
    //     if (!re.test(file.files[0].name.toLowerCase())) {
    //         this.setState({
    //             errMsg: '图片不符合，请重新选择!'
    //         });
    //         $('#croperrmsg').html('图片不符合，请重新选择!');
    //         return;
    //     }
    //     this.setState({
    //         errMsg: ''
    //     })
    //     let files;
    //     if (e.dataTransfer) {
    //       files = e.dataTransfer.files;
    //     } else if (e.target) {
    //       files = e.target.files;
    //     }
    //     const reader = new FileReader();
    //     reader.onload = () => {
    //       this.setState({ src: reader.result });
    //     };
    //     reader.readAsDataURL(files[0]);
    //   }
    
      cropImage(e) {
        e.preventDefault();
        if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
          return;
        }
        if (this.state.src == '') {
            return ;
        }
        this.setState({
          cropResult: this.cropper.getCroppedCanvas().toDataURL('image/jpeg'),
        });
      }

      useImage() {
         this.props.handResult(this.state.cropResult);
      }
    
      useDefaultImage(e) {
        e.preventDefault();
        this.setState({ src });
      }

    //   下拉框
    hideRenewal2(event) {
        // console.log(event.newValue, this.state.codetable.codes);
        let newValue = event.newValue;
        let newCropRatio =  this.state.cropRatio
        this.state.coprBox.forEach((item) => {
            if (newValue == item.id) {
                newCropRatio = item.ratio
                this.setState({ cropRatio: newCropRatio, policy: ''});
            }
        })
        
    }
    setSeleceData = () => {
        this.setState({selectModel:false})
    }
    // 获取自定义截框
    getCorpData = (e) => {
        let regx = /^([1-9]\/{1}[1-9]|[0-9]\s\/\s{1}[1-9])$/  
        let str = /^([1-9]\:{1}[1-9])$/
        let newCropRatio =  this.state.cropRatio
        let n1 = +e.newValue[0]
        let n2 = +e.newValue[e.newValue.length-1]

        if (regx.test(e.newValue)) {
            newCropRatio = eval(e.newValue)
            this.setState({ cropRatio: newCropRatio, policy: e.newValue,selectModel: true})
        } else if (str.test(e.newValue)) {
            newCropRatio = n1/n2
            this.setState({ cropRatio: newCropRatio, policy: e.newValue,selectModel: true})
        } else {
            UIMessageHelper.warning(r18n.formatNotMatch)
        }
    }
    renderValue = () => {
        return ''
    }
      render() {
        return (
          <div>
            <div style={{ width: '100%' }}>
              <div className="containeer">
                    <div className="row">
                        <div className="col-md-7">
                            <div className="img-container">
                                <label for="inputImage" style={{ height: 400, width: '100%', backgroundColor: '#eee', border: '1px dashed rgb(33, 150, 243)', textAlign: 'center', lineHeight: '400px', cursor: 'pointer', display: this.showCropper == 'none'? 'inherit':'none'}}>
                                    <input className="sr-only" id="inputImage" name="file"  onChange={this.onChangeCompress} type="file" accept="image/*" />
                                    <span className="rainbow PlusCircle" style={{ color: '#999', fontSize: '50px' }}></span>
                                </label>
                                <Cropper
                                    style={{ height: 400, width: '100%', backgroundColor: '#eee', border: '1px dashed rgb(33, 150, 243)', display: this.showCropper }}
                                    aspectRatio={this.state.cropRatio}
                                    preview=".cropper-img-preview"
                                    guides={false}
                                    src={this.state.src}
                                    ref={cropper => { this.cropper = cropper; }}
                                />
                                <div className="row" style={{'margin-top':'20px'}}>
                                    <div className="col-md-6"><span style={{color: 'red'}} id="croperrmsg">{this.state.errMsg}</span></div>
                                </div>              
                            </div>
                        </div>
                        <div className="col-md-2">
                            <div style={{ display: this.state.showCropButton && Util.parseBool(this.props.showRatio) ? 'block' : 'none'}}>
                                <UISelect
                                ignorePageReadOnly="true"
                                in="out"
                                label={r18n.proportionSelection}
                                codeTable={this.state.codetable}
                                model={this.createInfo}
                                property="id"
                                noI18n="true"
                                onOpen={this.setSeleceData}
                                valueRenderer={this.state.selectModel?this.renderValue:null}
                                onChange={this.hideRenewal2.bind(this)}/>
                            </div>
                            <div style={{ display: this.state.showCropButton && Util.parseBool(this.props.showRatio) ? 'block' : 'none'}}>
                            <UIText label={r18n.enterCustomScale} placeHolder='1 : 2'  onChange={this.getCorpData} isShowLabelTile="true" value={this.state.policy} showDeleteIcon="false" noI18n="true"/>
                            </div>
                            <label className="btn btn-default btn-upload" style={{textAlign: 'center', width: '80px', margin: '120px auto 0', display: this.state.showCropButton ? 'table' : 'none'}} for="inputImage"  title="Upload image file">
                                <input className="sr-only" id="inputImage" name="file"  onChange={this.onChangeCompress} type="file" accept="image/*" />
                                <span className="icon icon-upload">{r18n.reUpload}</span>
                            </label>
                            <button style={{'textAlign': 'center', 'margin': '40px auto 0px auto', display: this.state.showCropButton ? 'block' : 'none'}} onClick={this.cropImage} className="btn btn-primary">
                                {r18n.startCutting}
                            </button>
                            <div className="docs-preview clearfix">
                            {
                                this.state.cropResult && this.state.cropResult.length > 0 ?
                                <button onClick={this.useImage} style={{margin: '40px auto 0', display: 'block'}} className="btn btn-success">
                                    {r18n.usePhotos}
                                </button>
                                : ''
                            }
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="docs-preview clearfix">
                                <h1>
                                    <span style={{fontSize: '16px'}}>{r18n.preview}</span>
                                </h1>
                                <div className="cropper-box">
                                    
                                    {
                                        this.state.cropResult && this.state.cropResult.length > 0 ?
                                        <img style={{ "width": '100%' }} src={this.state.cropResult} alt="cropped image" id="cropresultimg" />
                                        : 
                                        <div style={{ "height": '100px', "width": '100px', backgroundColor: '#eee', border: '1px dashed rgb(33, 150, 243)'}}></div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
              
            </div>
          </div>
        );
      }
}

CropperUpload.propTypes = $.extend({}, Component.propTypes, {
	showRatio: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
});

CropperUpload.defaultProps = $.extend({}, Component.defaultProps, {
	componentType: "CropperUpload",
	showRatio: true
});
